[TOC]

# default-ci - repo with default CI/CD pipelines for all kind of projects

## Short description

1. Main purpose - manage availability and versions CI/CD pipelines from a single project
1. Limitations - current project strictly relies on [auto-release-semver](https://gitlab.com/codebricks/gitlab/ci/auto-release-semver) project
1. Other CI/CD pipelines rely on the presence of specific files or directories (Dockerfile, helmfile.d, etc)

## List of included CI/CD pipelines:

1. [auto-release-semver](https://gitlab.com/codebricks/gitlab/ci/auto-release-semver) - auto creation gitlab release and tag after merge
